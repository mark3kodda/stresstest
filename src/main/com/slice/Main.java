package main.com.slice;

import main.com.slice.mathop.MathFactory;
import main.com.slice.mathop.cache.MathFactoryCache;
import main.com.slice.mathop.impl.MathDivision;
import main.com.slice.mathop.impl.MathMinus;
import main.com.slice.mathop.impl.MathMultiply;
import main.com.slice.mathop.impl.MathPlus;
import main.com.slice.train.models.Train;
import main.com.slice.train.services.TrainComparator;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Train train1 = new Train("Paris", "1", 7.50, 50);
        Train train2 = new Train("Moscow", "2", 9.10, 40);
        Train train3 = new Train("Paris", "3", 7.30, 50);
        Train train4 = new Train("New-York", "4", 9.30, 40);//true
        Train train5 = new Train("New-York", "5", 7.00, 50);
        Train train6 = new Train("Berlin", "6", 9.10, 20);
        Train train7 = new Train("Paris", "7", 7.90, 50);


        List<Train> trains = Arrays.asList(train1, train2, train3, train4, train5, train6, train7);

        TrainComparator comparator = new TrainComparator();

        trains.sort(comparator);

        for (Train t: trains) {
            System.out.println(t);
        }

    }
}
